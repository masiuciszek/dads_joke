import React from 'react';
import './joke.css';

const Joke = ({ joke: { votes, text }, downVote, upVote }) => {
  const colourChange = () => {
    if (votes >= 12) {
      return 'rgba(46, 204, 113, 1)';
    }
    if (votes >= 9) {
      return '#eeee00';
    }
    if (votes >= 6) {
      return '#f0ff00';
    }
    if (votes >= 3) {
      return '#e67e22';
    }
    return '#f03434';
  };

  const smileChange = () => {
    if (votes >= 12) {
      return 'em em-sunglasses';
    }
    if (votes >= 9) {
      return 'em em-smile';
    }
    if (votes >= 6) {
      return 'em em-worried';
    }
    if (votes >= 3) {
      return 'em em-cry';
    }
    return 'em em-crying_cat_face';
  };

  return (
    <div className="joke">
      <div className="joke-buttons">
        <i className="fas fa-arrow-up" onClick={upVote} />
        <span className="vote" style={{ borderColor: colourChange() }}>
          {votes}
        </span>
        <i className="fas fa-arrow-down" onClick={downVote} />
      </div>
      <section className="joke-section">
        <h3>{text}</h3>
      </section>
      <aside className="exression">
        {' '}
        <i className={smileChange()} />
      </aside>
    </div>
  );
};

export default Joke;
