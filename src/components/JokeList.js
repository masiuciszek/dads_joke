/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import Joke from './Joke';
import axios from 'axios';
import img from '../img/happy.svg';
import uuid from 'uuid/v4';
import './jokelist.css';

function JokeList(props) {
  const [jokes, setJokes] = useState(
    JSON.parse(window.localStorage.getItem('jokes') || '[]')
  );
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (jokes.length === 0) {
      getJokes();
    }
  }, []);

  let seenJokes = new Set(jokes.map(j => j.text));

  const getJokes = async () => {
    try {
      const joke = [];
      while (joke.length < 10) {
        const res = await axios.get('https://icanhazdadjoke.com/', {
          headers: { Accept: 'application/json' }
        });
        let newJoke = res.data.joke;
        if (!seenJokes.has(newJoke)) {
          joke.push({ id: uuid(), text: newJoke, votes: 0 });
        } else {
          console.log('found a dublicate!');
          console.log(newJoke);
        }
      }
      setJokes(
        [...jokes, ...joke],
        () => window.localStorage.setItem('jokes'),
        JSON.stringify(jokes)
      );
      setLoading(false);
      window.localStorage.setItem('jokes', JSON.stringify(joke));
    } catch (err) {
      console.log(err.message);
      setLoading(false);
    }
  };

  const handleVote = (id, detla) => {
    setJokes(
      jokes.map(
        j => (j.id === id ? { ...j, votes: j.votes + detla } : j),
        () => window.localStorage.setItem('jokes', JSON.stringify(jokes))
      )
    );
  };

  const handleClick = () => {
    setLoading(true, getJokes());
  };

  const renderJokes = () => {
    let sortedJokes = jokes.sort((a, b) => b.votes - a.votes);
    return sortedJokes.map(j => (
      <Joke
        key={j.id}
        joke={j}
        upVote={() => handleVote(j.id, +1)}
        downVote={() => handleVote(j.id, -1)}
      />
    ));
  };

  return (
    <div className="jokelist">
      <aside className="side">
        <h1>
          <span className="dads">Dad's</span> Jokes{' '}
        </h1>
        <img src={img} alt="smile" />
        <button type="button" className="btn" onClick={handleClick}>
          Fetch Jokes
        </button>
      </aside>
      <div className="jokes-area">
        {loading ? (
          <div className="loading">
            <h1>Loding...</h1>
            <i className="far fa-8x fa-laugh fa-spin" />
          </div>
        ) : (
          renderJokes()
        )}
      </div>
    </div>
  );
}

JokeList.defaultProps = {
  limitJokes: 10
};
export default JokeList;
