import React from 'react';
import JokeList from './components/JokeList';
import './App.css';
export default function App() {
  return (
    <div className="App">
      <JokeList />
    </div>
  );
}
